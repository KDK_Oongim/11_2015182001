#include "stdafx.h"
#include "ScnMgr.h"
#include "Dependencies\freeglut.h"
#include <float.h>


int g_IdleTex = -1;
int g_RunTex = -1;
int g_JumpTex = -1;

int g_testTex = -1;
int g_bulletTex = -1;
int g_TestAnimTex = -1;
int g_BGTex = -1;
int g_BG2Tex = -1;
int g_FenceTex = -1;

int g_BGM = -1;
int g_FIRE = -1;
int g_EXPL = -1;

int g_particle = -1;
int g_particleTex = -1;
int g_RunParticle = -1;

int PlayerRank = 0;
int g_RankingTex = -1;
int g_FinishLineTex = -1;

int g_NumberTex = -1;
int g_GoTex = -1;
int g_GoCount = 0;

int g_TitleTex = -1;
ScnMgr::ScnMgr()
{
	// Initialize Renderer
	m_Renderer = new Renderer(1000, 1000);		// 10x10m 방
	if (!m_Renderer->IsInitialized())
	{
		std::cout << "Renderer could not be initialized.. \n";
	}

	//Initialize objects
	for (int i = 0; i < MAX_OBJ_COUNT; ++i)
	{
		m_Obj[i] = NULL;
	}

	m_Physics = new Physics;

	m_Sound = new Sound();
	//가급적 수동으로 지우는건 지양하지만, 초기화 할 때는 괜찮다.
	g_IdleTex = m_Renderer->GenPngTexture("Image/Idle.png");
	g_RunTex = m_Renderer->GenPngTexture("Image/Run.png");
	g_JumpTex = m_Renderer->GenPngTexture("Image/Jump.png");

	g_BGTex = m_Renderer->GenPngTexture("Image/Map1.png");
	g_BG2Tex = m_Renderer->GenPngTexture("Image/BackGround.png");
	g_FenceTex = m_Renderer->GenPngTexture("Image/Fence.png");
	g_FinishLineTex = m_Renderer->GenPngTexture("Image/FinishLine.png");
	g_NumberTex = m_Renderer->GenPngTexture("Image/Number.png");
	g_GoTex = m_Renderer->GenPngTexture("Image/Go.png");
	g_TitleTex = m_Renderer->GenPngTexture("Image/Title.png");

	g_particleTex = m_Renderer->GenPngTexture("Image/particle.png");


	g_bulletTex = m_Renderer->GenPngTexture("Image/Bullet.png");

	g_RankingTex = m_Renderer->GenPngTexture("Image/Ranking.png");

	g_BGM = m_Sound->CreateBGSound("./Sounds/BLACK BOX - Complex.mp3");
	m_Sound->PlayBGSound(g_BGM, true, 1.f);
	g_FIRE = m_Sound->CreateShortSound("./Sounds/BULLET.mp3");
	g_EXPL = m_Sound->CreateShortSound("./Sounds/COLLISION.mp3");

	g_particle = m_Renderer->CreateParticleObject(
		10000,
		5000, 2100,
		-5000, -2100,
		0, 0,
		10, 10,
		-100, -100,
		-50, -50
	);
	g_RunParticle = m_Renderer->CreateParticleObject(
		50,
		5, 5,
		-5, -5,
		5, 5,
		10, 10,
		0, -5,
		5, 5
	);
	m_Obj[HERO_ID] = new Horse(g_IdleTex, g_RunTex, g_JumpTex);
	m_Obj[HERO_ID]->SetPos(-28.5f, 16.5f, 0);
	m_Obj[HERO_ID]->SetVol(1.5, 1, 0.5);
	m_Obj[HERO_ID]->SetColor(1, 1, 1, 1);
	m_Obj[HERO_ID]->SetVel(0, 0, 0);
	m_Obj[HERO_ID]->SetMass(1);	//1kg
	m_Obj[HERO_ID]->SetFricCoef(0.7);
	m_Obj[HERO_ID]->SetType(TYPE_NORMAL);
	m_Obj[HERO_ID]->SetHP(1000);
	m_Obj[HERO_ID]->SetTexID(g_TestAnimTex);

	for (int i = 1; i < 4; ++i) {
		m_Obj[i] = new Horse(g_IdleTex, g_RunTex, g_JumpTex, 3 + i);
		m_Obj[i]->SetPos(-28.5f + i, 16.5f + 1.5 * i, 0);
		m_Obj[i]->SetVol(1.5, 1, 0.5);
		m_Obj[i]->SetColor(1, 1, 1, 1);
		m_Obj[i]->SetVel(0, 0, 0);
		m_Obj[i]->SetMass(5);	//1kg
		m_Obj[i]->SetFricCoef(0.7);
		m_Obj[i]->SetType(TYPE_AI);
		m_Obj[i]->SetHP(1000);
		m_Obj[i]->SetTexID(g_TestAnimTex);
	}

	for (int i = 0; i < MAX_OBJ_COUNT; ++i)
	{
		if (m_Obj[i] != NULL)
			m_Obj[i]->Update(0.f);
	}
}


ScnMgr::~ScnMgr()
{
	if (m_Renderer != NULL)
	{
		delete m_Renderer;
		m_Renderer = NULL;
	}
	if (m_Physics != NULL)
	{
		delete m_Physics;
		m_Physics = NULL;
	}

}


void ScnMgr::RenderScene(float ElapsedTime)
{
	if (CurrentScene == Title_Scene) {
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glClearColor(0, 1, 0, 1.0f);

		m_Renderer->DrawGround(
			0.f, 0.f, 0.f,
			1000, -800, 0.f,
			1, 1, 1, 1,
			g_TitleTex,
			1.f
		);
	}
	else {
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glClearColor(0, 0, 0, 1.0f);
		//draw background first
		m_Renderer->DrawGround(
			0.f, 0.f, 0.f,
			12000.f, 4000.f, 0.f,
			1, 1, 1, 1,
			g_BG2Tex,
			1.f
		);
		//draw background second
		m_Renderer->DrawGround(
			0.f, 0.f, 0.f,
			1280.f * 8, 720.f * 8, 0.f,
			1, 1, 1, 1,
			g_BGTex,
			1.f
		);

		static float pTime = 0.f;
		pTime += 0.016f;
		m_Renderer->DrawParticle(g_particle,
			0, 0, 0,
			1,
			1, 1, 1, 1,
			-50, -50,
			g_particleTex,
			1, pTime);
		for (int i = 0; i < MAX_OBJ_COUNT; ++i)
		{
			if (m_Obj[i] != NULL)
			{
				float x, y, z = 0;
				float sx, sy, sz = 0;
				float r, g, b, a = 0;
				float vx, vy, vz = 0;
				m_Obj[i]->GetPos(&x, &y, &z);
				m_Obj[i]->GetVol(&sx, &sy, &sz);
				m_Obj[i]->GetVel(&vx, &vy, &vz);
				m_Obj[i]->GetColor(&r, &g, &b, &a);

				//1m = 100cm = 100pixels
				x = x * 100.f;	sx = sx * 100.f;
				y = y * 100.f;	 sy = sy * 100.f;
				z = z * 100.f;	 sz = sz * 100.f;

				int texID = -1;
				m_Obj[i]->GetTexID(&texID);

				if (i < 4)
				{
					float gauge = (m_Obj[i]->CurrentProgress * 100 / 78);
					m_Renderer->DrawSolidRectGauge(
						x, y, z,
						0, sy / 2.f, 0,
						sx, 5, 0,
						1, 0, 0, 1,
						gauge);
					if (texID == g_IdleTex) {
						if (!m_Obj[i]->isRight)sx *= -1;
						m_Renderer->DrawParticle(g_RunParticle,
							x, y, z,
							1,
							0, 0, 0, 1,
							-vx, -vy,
							g_particleTex,
							1, pTime);
						m_Renderer->DrawTextureRectAnim(
							x, y, z,
							sx, sy, sz,
							r, g, b, a,
							texID,
							1,
							1,
							0,
							0);

					}
					else if (texID == g_RunTex) {
						if (vx < 0) { sx *= -1; }
						int count;
						m_Obj[i]->GetSpriteCount(&count);
						int iX = count % 6;
						m_Renderer->DrawParticle(g_RunParticle,
							x, y, z,
							1,
							0.6, 0.45, 0.12, 1,
							-vx, -vy,
							g_particleTex,
							1, pTime);
						m_Renderer->DrawTextureRectAnim(
							x, y, z,
							sx, sy, sz,
							r, g, b, a,
							texID,
							6,
							1,
							iX,
							0);

					}
					else if (texID == g_JumpTex) {
						if (vx < 0) { sx *= -1; }
						int iX = 2;
						if (vz > 0 && z > 0) iX = 0;
						if (vz > 0 && z > 0.5 * 100) iX = 1;
						if (vz < 0 && z < 1 * 100)iX = 3;
						if (vz < 0 && z < 0.5 * 100)iX = 4;

						m_Renderer->DrawTextureRectAnim(
							x, y, z,
							sx, sy, sz,
							r, g, b, a,
							texID,
							5,
							1,
							iX,
							0);
					}

				}
				else
				{
					if (vx < 0) { sx *= -1; }
					m_Renderer->DrawTextureRect(x, y, z, sx, sy, sz, r, g, b, a, texID);
				}
			}
		}

		m_Renderer->DrawTextureRect(
			0, 720.f * 4 + 48, 0,
			1280.f * 8, -720.f * 8, 0.f,
			1, 1, 1, 1,
			g_FenceTex,
			false
		);
		if (g_GoCount < 4) {
			float x, y, z = 0;
			m_Obj[HERO_ID]->GetPos(&x, &y, &z);
			if (g_GoCount < 3) {
				m_Renderer->DrawTextureRectAnim(
					x * 100, y * 100 + 200, z,
					233, 300, 0.f,
					1, 1, 1, 1,
					g_NumberTex,
					3,
					1,
					g_GoCount,
					0);
			}
			else {
				m_Renderer->DrawTextureRect(
					x * 100, y * 100 + 500, z,
					500, -300, 0.f,
					1, 1, 1, 0.5,
					g_GoTex,
					false
				);
			}
		}
	}
	if (m_Obj[HERO_ID]->CurrentProgress > 70) {
		m_Renderer->DrawTextureRect(
			2080, 1600, 0,
			20, 500, 0.f,
			1, 1, 1, 1,
			g_FinishLineTex,
			false
		);
	}
	if (CurrentScene == Ending_Scene) {
		float x, y, z = 0;
		m_Obj[HERO_ID]->GetPos(&x, &y, &z);
		m_Renderer->DrawTextureRectAnim(
			x * 100, y * 100 + 200, z,
			600, 300, 0.f,
			1, 1, 1, 1,
			g_RankingTex,
			2,
			2,
			(PlayerRank - 1) % 2,
			(PlayerRank - 1) / 2);
	}
}


int ScnMgr::AddObject(float x, float y, float z,
	float sx, float sy, float sz,
	float r, float g, float b, float a,
	float vx, float vy, float vz,
	float mass,
	float fricCoef,
	float hp,
	int type)
{
	// TEST OBJECT CLASS
	int idx = -1;

	for (int i = 0; i < MAX_OBJ_COUNT; ++i)
	{
		if (m_Obj[i] == NULL)
		{
			idx = i;
			break;
		}
	}
	if (idx == -1)
	{
		std::cout << "No more empty obj slot. " << std::endl;
		return -1;
	}

	m_Obj[idx] = new Object();
	m_Obj[idx]->SetPos(x, y, z);
	m_Obj[idx]->SetVol(sx, sy, sz);
	m_Obj[idx]->SetColor(r, g, b, a);
	m_Obj[idx]->SetVel(vx, vy, vz);
	m_Obj[idx]->SetMass(mass);
	m_Obj[idx]->SetFricCoef(fricCoef);
	m_Obj[idx]->SetType(type);
	m_Obj[idx]->SetHP(hp);

	m_Obj[idx]->SetTexID(g_bulletTex);
	return idx;

}

void ScnMgr::DeleteObject(int idx)
{
	if (idx < 0)
	{
		std::cout << "input idx is negative : " << idx << std::endl;
		return;
	}

	if (idx >= MAX_OBJ_COUNT)
	{
		std::cout << "input idx exceeds MAX_OBJ_COUNT : " << idx << std::endl;
		return;
	}

	if (m_Obj[idx] == NULL)
	{
		std::cout << "m_Obj[" << idx << "] is NULL" << std::endl;
		return;
	}

	delete m_Obj[idx];
	m_Obj[idx] = NULL;
	return;
}

Object* ScnMgr::GetObj() const
{
	return *m_Obj;
}

void ScnMgr::CheckProgress(Object* object)
{
	float x, y, z = 0;
	object->GetPos(&x, &y, &z);
	x *= 100;
	y *= 100;
	z *= 100;

	switch (object->CurrentProgress % 24)
	{
	case 0:
		if (x > -2000)
			object->CurrentProgress++;
		break;
	case 1:
		if (x > -1200)
			object->CurrentProgress++;
		break;
	case 2:
		if (x > -400)
			object->CurrentProgress++;
		break;
	case 3:
		if (x > 400)
			object->CurrentProgress++;
		break;
	case 4:
		if (x > 1200)
			object->CurrentProgress++;
		break;
	case 5:
		if (x > 2000)
			object->CurrentProgress++;
		break;
	case 6:
		if (x > 2800)
			object->CurrentProgress++;
		break;
	case 7:
		if (x > 3800)
			object->CurrentProgress++;
		break;
	case 8:
		if (y < 800)
			object->CurrentProgress++;
		break;
	case 9:
		if (y < 0)
			object->CurrentProgress++;
		break;
	case 10:
		if (y < -1100)
			object->CurrentProgress++;
		break;
	case 11:
		if (x < 2800)
			object->CurrentProgress++;
		break;
	case 12:
		if (x < 2000)
			object->CurrentProgress++;
		break;
	case 13:
		if (x < 1200)
			object->CurrentProgress++;
		break;
	case 14:
		if (x < 400)
			object->CurrentProgress++;
		break;
	case 15:
		if (x < -400)
			object->CurrentProgress++;
		break;
	case 16:
		if (x < -1200)
			object->CurrentProgress++;
		break;
	case 17:
		if (x < -2000)
			object->CurrentProgress++;
		break;
	case 18:
		if (x < -2800)
			object->CurrentProgress++;
		break;
	case 19:
		if (x < -4300)
			object->CurrentProgress++;
		break;
	case 20:
		if (y > 0)
			object->CurrentProgress++;
		break;
	case 21:
		if (y > 1100)
			object->CurrentProgress++;
		break;
	case 22:
		if (y > 1700)
			object->CurrentProgress++;
		break;
	case 23:
		if (x > -2900)
			object->CurrentProgress++;
		break;
	}
}

void ScnMgr::Update(float ElapsedTime)
{
	if (CurrentScene == Play_Scene) {
		static float pTime = 0.f;
		if (g_GoCount < 3)
		{
			pTime += ElapsedTime;
			if (pTime > 1.f) {
				pTime = 0;
				g_GoCount++;
			}
		}
		else {
			if (g_GoCount == 3)
			{
				pTime += ElapsedTime;
				if (pTime > 1.f) {
					pTime = 0;
					g_GoCount++;
				}
			}
			float fx, fy, fz;
			fx = fy = fz = 0.f;
			float fAmount = 10.f;
			if (m_KeyW)
			{
				fy += 1.f;
			}
			if (m_KeyS)
			{
				fy -= 1.f;
			}
			if (m_KeyD)
			{
				fx += 1.f;
			}
			if (m_KeyA)
			{
				fx -= 1.f;
			}
			if (m_KeySP)
			{
				fz += 1.f;
			}

			// Add control force to hero
			float fsize = sqrtf(fx * fx + fy * fy);
			if (fsize > FLT_EPSILON)
			{
				fx /= fsize;
				fy /= fsize;
				fx *= fAmount;
				fy *= fAmount;

				m_Obj[HERO_ID]->AddForce(fx, fy, 0.f, ElapsedTime);
			}
			if (fz > FLT_EPSILON)
			{
				float x, y, z;
				m_Obj[HERO_ID]->GetPos(&x, &y, &z);

				if (z < FLT_EPSILON)
				{
					fz *= fAmount * 50.f;
					m_Obj[HERO_ID]->AddForce(0.f, 0.f, fz, ElapsedTime);
				}

			}

			//Fire bullets
			if (m_Obj[HERO_ID]->CanShootBullet())
			{
				float bulletVel = 5.f;
				float vBulletX, vBulletY, vBulletZ;
				vBulletX = vBulletY = vBulletZ = 0.f;

				if (m_KeyUp)
					vBulletY += 1.f;
				if (m_KeyLeft)
					vBulletX -= 1.f;
				if (m_KeyRight)
					vBulletX += 1.f;
				if (m_KeyDown)
					vBulletY -= 1.f;

				float vBulletSize = sqrtf(vBulletX * vBulletX + vBulletY * vBulletY + vBulletZ * vBulletZ);
				if (vBulletSize > FLT_EPSILON)
				{
					//create bullet
					//normalize
					vBulletX /= vBulletSize;
					vBulletY /= vBulletSize;
					vBulletZ /= vBulletSize;
					//실제 속도
					vBulletX *= bulletVel;
					vBulletY *= bulletVel;
					vBulletZ *= bulletVel;

					float hx, hy, hz;
					float hvx, hvy, hvz;
					m_Obj[HERO_ID]->GetPos(&hx, &hy, &hz);
					m_Obj[HERO_ID]->GetVel(&hvx, &hvy, &hvz);

					vBulletX += hvx;
					vBulletY += hvy;
					vBulletZ += hvz;

					int num = AddObject(hx, hy, hz,
						0.3, 0.3, 0.3,
						1, 1, 1, 1,
						vBulletX, vBulletY, vBulletZ,
						1.f,
						0.1f,
						2.f,
						TYPE_BULLET);
					m_Obj[num]->SetParentObj(m_Obj[HERO_ID]);
					m_Obj[HERO_ID]->ResetBulletCoolTime();
					m_Sound->PlayShortSound(g_FIRE, false, 1.f);
				}
			}
			for (int i = 1; i < 4; ++i) {
				if (m_Obj[i]->CanShootBullet())
				{

					float bulletVel = 3.f;
					float vBulletX, vBulletY, vBulletZ;
					float TargetX, TargetY, TargetZ;
					float hx, hy, hz;
					float hvx, hvy, hvz;
					m_Obj[i]->GetPos(&hx, &hy, &hz);
					m_Obj[i]->GetVel(&hvx, &hvy, &hvz);

					vBulletX = vBulletY = vBulletZ = 0.f;
					m_Obj[HERO_ID]->GetPos(&TargetX, &TargetY, &TargetZ);
					vBulletX = TargetX - hx;
					vBulletY = TargetY - hy;

					float vBulletSize = sqrtf(vBulletX * vBulletX + vBulletY * vBulletY + vBulletZ * vBulletZ);
					if (vBulletSize > FLT_EPSILON)
					{
						//create bullet
						//normalize
						vBulletX /= vBulletSize;
						vBulletY /= vBulletSize;
						vBulletZ /= vBulletSize;
						//실제 속도
						vBulletX *= bulletVel;
						vBulletY *= bulletVel;
						vBulletZ *= bulletVel;

						vBulletX += hvx;
						vBulletY += hvy;
						vBulletZ += hvz;

						int num = AddObject(hx, hy, hz,
							0.3, 0.3, 0.3,
							1, 1, 1, 1,
							vBulletX, vBulletY, vBulletZ,
							0.9f,
							0.1f,
							2.f,
							TYPE_BULLET);
						m_Obj[num]->SetParentObj(m_Obj[i]);
						m_Obj[i]->ResetBulletCoolTime();
						m_Sound->PlayShortSound(g_FIRE, false, 1.f);
					}
				}
			}

			//Progress Check
			for (int i = 0; i < 4; ++i)
			{
				CheckProgress(m_Obj[i]);
				if (m_Obj[i]->CurrentProgress == 78)
				{
					m_Obj[i]->CurrentProgress++;
					PlayerRank++;
					if (i == HERO_ID)
						CurrentScene = Ending_Scene;
					std::cout << PlayerRank << std::endl;
				}

			}

			//Overlaptest
			for (int src = 0; src < MAX_OBJ_COUNT; ++src)
			{
				if (m_Obj[src] != NULL)
				{
					float x, y, z;
					m_Obj[src]->GetPos(&x, &y, &z);
					x *= 100;
					y *= 100;
					z *= 100;
					if ((x<2850 && x>-2850 && y<1600 && y>-1600) || y > 2100 || y < -2100)
					{
						m_Physics->ProcessWallCollision(m_Obj[src]);
						if (m_Obj[src]->isPrevOverlap)
							m_Obj[src]->overlapCount++;
						else
							m_Obj[src]->isPrevOverlap = true;
					}
					float dis = pow(x - 2900, 2) + pow(y - 10, 2);
					//std::cout << sqrt(dis) << std::endl;
					if (1600 * 1600 > dis ||
						(2050 * 2050 < dis && x > 2850 && y >= 0) ||
						(2100 * 2100 < dis && x > 2850 && y < 0)) {

						m_Physics->ProcessWallCollision(m_Obj[src]);
						if (m_Obj[src]->isPrevOverlap)
							m_Obj[src]->overlapCount++;
						else
							m_Obj[src]->isPrevOverlap = true;
					}
					dis = pow(x - (-2900), 2) + pow(y - 10, 2);
					if (1600 * 1600 > dis ||
						(2050 * 2050 < dis && x < -2850 && y >= 0) ||
						(2100 * 2100 < dis && x < -2850 && y < 0)) {

						m_Physics->ProcessWallCollision(m_Obj[src]);
						if (m_Obj[src]->isPrevOverlap)
							m_Obj[src]->overlapCount++;
						else
							m_Obj[src]->isPrevOverlap = true;
					}
					for (int dst = src + 1; dst < MAX_OBJ_COUNT; ++dst)
					{
						if (m_Obj[src] != NULL && m_Obj[dst] != NULL)
						{
							if (m_Physics->IsOverlap(m_Obj[src], m_Obj[dst]))
							{
								std::cout << "Collision : ("
									<< src << ", " << dst << ")" << std::endl;
								if (!m_Obj[src]->IsAncestor(m_Obj[dst]) && !m_Obj[dst]->IsAncestor(m_Obj[src])) {
									m_Physics->ProcessCollision(m_Obj[src], m_Obj[dst]);

									if (m_Obj[src]->isPrevOverlap)
										m_Obj[src]->overlapCount++;
									else
										m_Obj[src]->isPrevOverlap = true;

									m_Sound->PlayShortSound(g_EXPL, false, 1.f);
								}
							}
						}
					}
				}
			}

			for (int i = 0; i < MAX_OBJ_COUNT; ++i)
			{
				if (m_Obj[i] != NULL)
					m_Obj[i]->Update(ElapsedTime);
			}
		}
		float x, y, z;
		m_Obj[HERO_ID]->GetPos(&x, &y, &z);
		m_Renderer->SetCameraPos(x * 100, y * 100);
	}
}

void ScnMgr::DoGarbageCollection()
{
	for (int i = 0; i < MAX_OBJ_COUNT; ++i)
	{
		if (m_Obj[i] != NULL)
		{
			int type;
			m_Obj[i]->GetType(&type);
			if (type == TYPE_BULLET)
			{
				if (m_Obj[i]->overlapCount > 25) {
					DeleteObject(i);
					continue;
				}
				else {
					float vx, vy, vz;
					m_Obj[i]->GetVel(&vx, &vy, &vz);
					float vSize = sqrtf(vx * vx + vy * vy + vz * vz);
					if (vSize < FLT_EPSILON) {
						DeleteObject(i);
						continue;
					}
				}
			}

			float hp;
			m_Obj[i]->GetHP(&hp);
			if (hp < FLT_EPSILON)
			{
				DeleteObject(i);
				continue;
			}
		}
	}
}

//디버그는 호출하는 놈을 찾아야 한다.	--> 호출 스택

void ScnMgr::KeyDownInput(unsigned char key, int x, int y)
{
	if (CurrentScene == Title_Scene) {
		if (key == 13)
			CurrentScene = Play_Scene;
	}
	if (CurrentScene == Play_Scene) {
		if (key == 'w' || key == 'W')
		{
			m_KeyW = true;
		}
		if (key == 'a' || key == 'A')
		{
			m_KeyA = true;
			m_Obj[HERO_ID]->isRight = false;
		}
		if (key == 's' || key == 'S')
		{
			m_KeyS = true;
		}
		if (key == 'd' || key == 'D')
		{
			m_KeyD = true;
			m_Obj[HERO_ID]->isRight = true;
		}
		if (key == 'r' || key == 'R')
		{
			static float ResetPos[24][2] = {
			{-2000,1800},
			{-1200,1800},
			{-400,1800},
			{400,1800},
			{1200,1800},
			{2000,1800},
			{2800,1800},

			{3800,1500},
			{4500,800},
			{4800,0},
			{4420,-1100},
			{2800,-1750},

			{2000,-1800},
			{1200,-1800},
			{400,-1800},
			{-400,-1800},
			{-1200,-1800},
			{-2000,-1800},

			{-2800,-1800},
			{-4300,-1200},
			{-4700,0},
			{-4400,1100},
			{-3550,1700},
			{-2900,1650},
			};
			if (m_Obj[HERO_ID]->CurrentProgress == 0)
				m_Obj[HERO_ID]->SetPos(-28.5f, 16.5, 1);
			else m_Obj[HERO_ID]->SetPos(ResetPos[(m_Obj[HERO_ID]->CurrentProgress - 1) % 24][0] / 100,
				ResetPos[(m_Obj[HERO_ID]->CurrentProgress - 1) % 24][1] / 100, 1);
			m_Obj[HERO_ID]->SetVel(0.f, 0.f, 0.f);
		}
		if (key == ' ')
		{
			m_KeySP = true;
		}
	}
	if (CurrentScene == Ending_Scene) {
		if (key == 27)
			exit(0);
	}
}

void ScnMgr::KeyUpInput(unsigned char key, int x, int y)
{
	if (key == 'w' || key == 'W')
	{
		m_KeyW = false;
	}
	if (key == 'a' || key == 'A')
	{
		m_KeyA = false;
	}
	if (key == 's' || key == 'S')
	{
		m_KeyS = false;
	}
	if (key == 'd' || key == 'D')
	{
		m_KeyD = false;
	}
	if (key == ' ')
	{
		m_KeySP = false;
	}
}

void ScnMgr::SpecialKeyDownInput(int key, int x, int y)
{
	
	if (key == GLUT_KEY_UP)
	{
		m_KeyUp = true;
	}
	if (key == GLUT_KEY_LEFT)
	{
		m_KeyLeft = true;
	}
	if (key == GLUT_KEY_DOWN)
	{
		m_KeyDown = true;
	}
	if (key == GLUT_KEY_RIGHT)
	{
		m_KeyRight = true;
	}
}

void ScnMgr::SpecialKeyUpInput(int key, int x, int y)
{
	if (key == GLUT_KEY_UP)
	{
		m_KeyUp = false;
	}
	if (key == GLUT_KEY_LEFT)
	{
		m_KeyLeft = false;
	}
	if (key == GLUT_KEY_DOWN)
	{
		m_KeyDown = false;
	}
	if (key == GLUT_KEY_RIGHT)
	{
		m_KeyRight = false;
	}
}




