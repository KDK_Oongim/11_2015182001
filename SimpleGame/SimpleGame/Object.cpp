#include "stdafx.h"
#include "Object.h"
#include "Globals.h"
#include <algorithm>
#include <float.h>
#include <iostream>
Object::Object()
{
	InitPhysics();
}

Object::~Object()
{

}

void Object::Update(float ElapsedTime)
{
	// 쿨타임 줄이기
	m_RemainingCoolTime -= ElapsedTime;


	//마찰 계수 적용
	float nForce = m_mass * GRAVITY; //scalar
	float fForce = m_fricCoef * nForce;	//scalar
	float velSize = sqrtf(m_velX*m_velX + m_velY * m_velY+m_velZ* m_velZ);
	if (velSize > 0.f)
	{
		float fDirX = -1.f * m_velX / velSize;
		float fDirY = -1.f * m_velY / velSize;
		fDirX = fDirX * fForce;
		fDirY = fDirY * fForce;
		float fAccX = fDirX / m_mass;
		float fAccY = fDirY / m_mass;
		float newX = m_velX + fAccX * ElapsedTime;
		float newY = m_velY + fAccY * ElapsedTime;
		if (newX*m_velX < 0.f)
		{
			m_velX = 0.f;
		}
		else
		{
			m_velX = newX;
		}
		if (newY*m_velY < 0.f)
		{
			m_velY = 0.f;
		}
		else
		{
			m_velY = newY;
		}
		m_velZ = m_velZ - GRAVITY * ElapsedTime;
	}

	//위치
	m_posX = m_posX + m_velX * ElapsedTime;
	m_posY = m_posY + m_velY * ElapsedTime;
	m_posZ = m_posZ + m_velZ * ElapsedTime;

	if (m_posZ < FLT_EPSILON)
	{
		m_posZ = 0.f;
		m_velZ = 0.f;
	}

	velSize = sqrtf(pow(m_velX, 2)+pow(m_velY, 2));
	AddSpriteCount(velSize * ElapsedTime);
}

void Object::AddForce(float x, float y, float z, float ElapsedTime)
{
	float accX, accY, accZ;
	accX = accY = accZ = 0.f;

	accX = x / m_mass;
	accY = y / m_mass;
	accZ = z / m_mass;

	m_velX = m_velX + accX * ElapsedTime;
	m_velY = m_velY + accY * ElapsedTime;
	m_velZ = m_velZ + accZ * ElapsedTime;
}

void Object::InitPhysics()
{
	float default_setting = 0;

	m_posX = m_posY = m_posZ = default_setting;
	m_r = m_g = m_b = m_a = default_setting;
	m_posX = m_posY = m_posZ = default_setting;

	m_velX = m_velY = m_velZ = default_setting;
	m_accX = m_accY = m_accZ = default_setting;
	m_volX = m_volY = m_volZ = default_setting;
	m_mass = default_setting;
	m_fricCoef = default_setting;
}

bool Object::CanShootBullet()
{
	if (m_RemainingCoolTime < FLT_EPSILON)
		return true;

	return false;
}
void Object::ResetBulletCoolTime()
{
	m_RemainingCoolTime = m_CurrentCoolTime;
}

void Object::SetColor(float r, float g, float b, float a)
{
	m_r = r;
	m_g = g;
	m_b = b;
	m_a = a;
}
void Object::GetColor(float *r, float *g, float *b, float *a)
{
	*r = m_r;
	*g = m_g;
	*b = m_b;
	*a = m_a;
}

void Object::SetPos(float posX, float posY, float posZ)
{
	m_posX = posX;
	m_posY = posY;
	m_posZ = posZ;
}
void Object::GetPos(float *posX, float *posY, float *posZ)
{
	*posX = m_posX;
	*posY = m_posY;
	*posZ = m_posZ;
}

void Object::SetMass(float mass)
{
	m_mass = mass;
}
void Object::GetMass(float *mass)
{
	*mass = m_mass;
}

void Object::SetAcc(float accX, float accY, float accZ)
{
	m_accX = accX;
	m_accY = accY;
	m_accZ = accZ;
}
void Object::GetAcc(float *accX, float *accY, float *accZ)
{
	*accX = m_accX;
	*accY = m_accY;
	*accZ = m_accZ;
}

void Object::SetVel(float velX, float velY, float velZ)
{
	m_velX = velX;
	m_velY = velY;
	m_velZ = velZ;
}
void Object::GetVel(float *velX, float *velY, float *velZ)
{
	*velX = m_velX;
	*velY = m_velY;
	*velZ = m_velZ;
}

void Object::SetVol(float volX, float volY, float volZ)
{
	m_volX = volX;
	m_volY = volY;
	m_volZ = volZ;
}
void Object::GetVol(float *volX, float *volY, float *volZ)
{
	*volX = m_volX;
	*volY = m_volY;
	*volZ = m_volZ;
}

void Object::SetFricCoef(float coef)
{
	m_fricCoef = coef;
}
void Object::GetFricCoef(float *coef)
{
	*coef = m_fricCoef;
}

void Object::SetType(int type)
{
	m_type = type;
}
void Object::GetType(int *type)
{
	*type = m_type;
}

void Object::SetTexID(int id)
{
	m_texID= id;
}
void Object::GetTexID(int* id)
{
	*id = m_texID;
}

void Object::SetParentObj(Object* obj)
{
	m_parent = obj;
}
Object* Object::GetParentObj()
{
	return m_parent;
}
bool Object::IsAncestor(Object* obj)
{
	if (obj != NULL)
	{
		if (obj == m_parent)
			return true;
	}
	return false;
}

void Object::SetHP(float hp)
{
	m_healthPoint = hp;
}
void Object::GetHP(float* hp)
{
	*hp = m_healthPoint;
}
void Object::Damage(float damage)
{
	m_healthPoint = m_healthPoint - damage;
}

void Object::AddSpriteCount(float count)
{
	m_spriteCnt += count;
}
void Object::GetSpriteCount(int* count)
{
	*count = m_spriteCnt;
}

void Object::SetZeroSpriteCount()
{
	m_spriteCnt = 0;
}

void Object::SetCoolTime(int Cooltime)
{
	m_CurrentCoolTime = Cooltime;
}


Horse::Horse(int idleTex, int runTex, int jumpTex, float CoolTime)
{
	m_IdleTexture = idleTex;
	m_RunTexture = runTex;
	m_JumpTexture = jumpTex;
	SetCoolTime(CoolTime);
}

//====================================================================================================
void Horse::Update(float ElapsedTime)
{
	int type;
	GetType(&type);
	float velX, velY, velZ;
	float posX, posY, posZ;
	GetPos(&posX, &posY, &posZ);
	GetVel(&velX, &velY, &velZ);
	if (type == TYPE_AI)
	{
		if (overlapCount > 250) {
			overlapCount = 0;
			if (m_curTarget == 0)
				SetPos(-28.5f + 3, 16.5f, 0);
			SetPos((m_target[(m_curTarget-1) % 12][0])/100, m_target[(m_curTarget-1) % 12][1]/100,0);
		}
		posX *= 100;
		posY *= 100;
		if (velX > 10)
			velX = 10;
		else if (velX < -10)
			velX = -10;

		if (velY > 10)
			velY = 10;
		else if (velY < -10)
			velY = -10;
		SetVel(velX, velY,velZ);

		float fx, fy;
		fx = m_target[m_curTarget%12][0] - posX;
		fy = m_target[m_curTarget%12][1] - posY;
		if (250 * 250 > fx * fx + fy * fy)m_curTarget += 1;

		
		float fsize = sqrtf(fx * fx + fy * fy);
		if (fsize > FLT_EPSILON)
		{
			fx /= fsize;
			fy /= fsize;
			fx *= 9.f*5;
			fy *= 9.f*5;

			AddForce(fx, fy, 0.f, ElapsedTime);
		}
		//std::cout << velX << ", " << velY << std::endl;
	}
	else
	{
		if (velX > 12)
			velX = 12;
		else if (velX < -12)
			velX = -12;

		if (velY > 12)
			velY = 12;
		else if (velY < -12)
			velY = -12;

		if (velZ > 5)
			velZ = 5;

		SetVel(velX, velY, velZ);
	}
	Object::Update(ElapsedTime);

	float velSize = sqrtf(pow(velX, 2) + pow(velY, 2));
	AddSpriteCount(velSize * ElapsedTime);
	if (velSize <= FLT_EPSILON) {
		SetTexID(m_IdleTexture);
		SetZeroSpriteCount();
	}
	else
	{
		SetTexID(m_RunTexture);
	}

	if (posZ > FLT_EPSILON)
	{
		int count;
		GetSpriteCount(&count);
		//if(velZ> FLT_EPSILON&&count%5>3){ AddSpriteCount(-velSize * ElapsedTime); }
		SetTexID(m_JumpTexture);
	}
}
