#pragma once
#include "Object.h"

class Physics
{
public:
	Physics();
	~Physics();

	bool IsOverlap(Object* A, Object* B, int method=0);
	void ProcessCollision(Object* A, Object* B);
	void ProcessWallCollision(Object* A);

private:
	bool BBOverlapTest(Object* A, Object* B);
	
};

