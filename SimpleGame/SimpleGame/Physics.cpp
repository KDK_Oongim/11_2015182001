#include "stdafx.h"
#include "Physics.h"

Physics::Physics()
{

}
Physics::~Physics()
{

}
bool Physics::IsOverlap(Object* A, Object* B, int method)
{
	switch (method)
	{
	case 0:		//BB overlap test
		return BBOverlapTest(A, B);
		break;

	default:
		break;
	}
	return false;

}
bool Physics::BBOverlapTest(Object* A, Object* B)
{
	float x1, y1, z1;
	float sx1, sy1, sz1;
	float aMinX, aMinY, aMinZ;
	float aMaxX, aMaxY, aMaxZ;

	float x2, y2, z2;
	float sx2, sy2, sz2;
	float bMinX, bMinY, bMinZ;
	float bMaxX, bMaxY, bMaxZ;

	A->GetPos(&x1, &y1, &z1);
	A->GetVol(&sx1, &sy1, &sz1);
	B->GetPos(&x2, &y2, &z2);
	B->GetVol(&sx2, &sy2, &sz2);

	aMinX = x1 - sx1 / 2.f; aMaxX = x1 + sx1 / 2.f;
	aMinY = y1 - sy1 / 2.f; aMaxY = y1 + sy1 / 2.f;
	aMinZ = z1 - sz1 / 2.f; aMaxZ = z1 + sz1 / 2.f;

	bMinX = x2 - sx2 / 2.f; bMaxX = x2 + sx2 / 2.f;
	bMinY = y2 - sy2 / 2.f; bMaxY = y2 + sy2 / 2.f;
	bMinZ = z2 - sz2 / 2.f; bMaxZ = z2 + sz2 / 2.f;

	if (aMinX > bMaxX)
		return false;
	if (aMaxX < bMinX) // �ε������� ����
		return false;

	if (aMinY > bMaxY)
		return false;
	if (aMaxY < bMinY)
		return false;

	if (aMinZ > bMaxZ)
		return false;
	if (aMaxZ < bMinZ)
		return false;

	return true;
}
void Physics::ProcessCollision(Object* A, Object* B)
{
	//A object
	float aMass, aVX, aVY, aVZ;
	A->GetMass(&aMass);
	A->GetVel(&aVX, &aVY, &aVZ);
	float afvx, afvy, afvz;

	//B object
	float bMass, bVX, bVY, bVZ;
	B->GetMass(&bMass);
	B->GetVel(&bVX, &bVY, &bVZ);
	float bfvx, bfvy, bfvz;

	afvx = ((aMass - bMass) / (aMass + bMass)) * aVX + ((2.f * bMass) / (aMass + bMass)) * bVX;
	afvy = ((aMass - bMass) / (aMass + bMass)) * aVY + ((2.f * bMass) / (aMass + bMass)) * bVY;
	afvz = ((aMass - bMass) / (aMass + bMass)) * aVZ + ((2.f * bMass) / (aMass + bMass)) * bVZ;

	bfvx = ((aMass * 2.f) / (aMass + bMass)) * aVX + ((bMass - aMass) / (aMass + bMass)) * bVX;
	bfvy = ((aMass * 2.f) / (aMass + bMass)) * aVY + ((bMass - aMass) / (aMass + bMass)) * bVY;
	bfvz = ((aMass * 2.f) / (aMass + bMass)) * aVZ + ((bMass - aMass) / (aMass + bMass)) * bVZ;

	
	A->SetVel(afvx, afvy, afvz);
	B->SetVel(bfvx, bfvy, bfvz);
}

void Physics::ProcessWallCollision(Object* A)
{
	//A object
	float aMass, aVX, aVY, aVZ;
	A->GetMass(&aMass);
	A->GetVel(&aVX, &aVY, &aVZ);
	float afvx, afvy, afvz;
	float bMass = 1000;

	afvx = ((aMass - bMass) / (aMass + bMass)) * aVX;
	afvy = ((aMass - bMass) / (aMass + bMass)) * aVY;
	afvz = ((aMass - bMass) / (aMass + bMass)) * aVZ;

	A->SetVel(afvx, afvy, afvz);

}
