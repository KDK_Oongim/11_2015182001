#pragma once

class Object
{
	
public:
	Object();
	~Object();

	virtual void Update(float ElapsedTime);
	void AddForce(float x, float y, float z, float ElapsedTime);

	void InitPhysics();

	bool CanShootBullet();
	void ResetBulletCoolTime();

	void SetColor(float r, float g, float b, float a);
	void GetColor(float *r, float *g, float *b, float *a);
	void SetPos(float posX, float posY, float posZ);
	void GetPos(float *posX, float *posY, float *posZ);
	void SetAcc(float accX, float accY, float accZ);
	void GetAcc(float *accX, float *accY, float *accZ);
	void SetVel(float velX, float velY, float velZ);
	void GetVel(float *velX, float *velY, float *velZ);
	void SetVol(float volX, float volY, float volZ);
	void GetVol(float *volX, float *volY, float *volZ);
	void SetMass(float mass);
	void GetMass(float *mass);
	void SetFricCoef(float coef);
	void GetFricCoef(float *coef);
	void SetType(int type);
	void GetType(int *type);
	void SetTexID(int id);
	void GetTexID(int* id);
	

	void SetHP(float hp);
	void GetHP(float* hp);
	void Damage(float damage);

	bool IsAncestor(Object* obj);
	void SetParentObj(Object* obj);
	Object* GetParentObj();

	void AddSpriteCount(float count);
	void GetSpriteCount(int* count);
	void SetZeroSpriteCount();
	void SetCoolTime(int Cooltime);

	bool isRight = true;
	bool isPrevOverlap = false;
	int overlapCount = 0;
	int CurrentProgress=0;
private:
	float m_r, m_g, m_b, m_a;			//		색깔
	float m_posX, m_posY, m_posZ;	//		위치
	float m_velX, m_velY, m_velZ;		//		속도
	float m_accX, m_accY, m_accZ;		//		가속도
	float m_volX, m_volY, m_volZ;		//		부피
	float m_mass;							//		무게
	float m_fricCoef;						//		마찰계수
	int m_type;								//		오브젝트 타입
	int m_texID;
	float m_spriteCnt;
	
	float m_RemainingCoolTime = 3.f;
	float m_CurrentCoolTime = 5.0f;
	float m_healthPoint;

	


	Object* m_parent;
};


class Horse : public Object
{
public:
	Horse(int idleTex,int runTex,int jumpTex, float CoolTime=5.0f);
	~Horse();
	
	virtual void Update(float ElapsedTime);

	
private:
	int m_IdleTexture;
	int m_RunTexture;
	int m_JumpTexture;
	
	int m_curTarget=0;
	float m_target[12][2] = {
		{2800,1800},
		{3800,1500},
		{4500,800},
		{4800,0},
		{4420,-1100},
		{2850,-1750},

		{-2800,-1800},
		{-4300,-1200},
		{-4700,0},
		{-4400,1100},
		{-3550,1700},
		{-2900,1650},
	};
};